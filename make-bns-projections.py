import time
import os
import H5
import SimulationIO as SIO
import RegionCalculus as RC
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import yt
yt.enable_parallelism()

num_files=16
path_base='/scratch/sciteam/miller1/davids_simulations/LS220_M135135_LK'
file_template='bns-{0:>4}.s5'
path_template = path_base + '/' + file_template
projection_template = 'bns-it-{}-projection.png'
slice_template = 'bns-it-{}-slice.png'
domain_left_edge=[-1048,-1048,-24]
domain_dds=[8,8,8]
center=[0,0,0]
L=[0,0,1]
width=50
field='HYDROBASE::rho'
axis='z'
num_plots=0

def get_configurations(fpath):
    f = H5.H5File(fpath,H5.H5F_ACC_RDONLY)
    p = SIO.readProject(f)
    cnames = filter(lambda x: x != 'global',
                    list(p.configurations.iterkeys()))
    f.close()
    iterations = map(lambda x: int(x.split('.')[1].split('-')[0]),
                     cnames)
    return iterations,cnames

start = time.clock()
for i in range(num_files):
    fpath = path_template.format(i)
    its,cnms = get_configurations(fpath)
    for it,cname in zip(its,cnms):
        ds = yt.load(fpath,
                     domain_left_edge=domain_left_edge,
                     domain_dds=domain_dds,
                     configuration=cname)
        p = yt.OffAxisProjectionPlot(ds,L,field,center,width=width)
        p.set_log(field,False)
        if yt.is_root():
            p.save(projection_template.format(it))
        num_plots += 1
end = time.clock()
dt = end - start
if yt.is_root():
    print "Time for {} plots = {} seconds".format(num_plots,dt)
