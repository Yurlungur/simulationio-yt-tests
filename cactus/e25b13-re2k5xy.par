ActiveThorns = "time
		coordbase
		mol
		boundary
		spacemask
		symbase
		aeilocalinterp
		localinterp
		nanchecker
		constants
		ioutil
		initbase
		sphericalsurface
		carpet
		carpetlib
		carpetregrid2
		carpetreduce
		cartgrid3d
		carpetslab
		carpetinterp
		carpetinterp2
		triggerterminationmanual 
	  	CartesianCoordinates
		LoopControl
		CoreCollapseControl
		Slab
		TGRTensor
		TimerReport
    		carpetioascii 
    		carpetevolutionmask 
		carpetioscalar 
		carpetiohdf5 
		carpetiobasic
		admbase 
		staticconformal 
		admcoupling 
		coordgauge 
		admmacros 
		TmunuBase
		Dissipation
		HydroBase
		GRHydro
		GRHydro_InitData
		ZelmaniStarMapper
		eos_omni
		Formaline
		ZelmaniLeak
		ZelmaniShockTracker2
		ZelmaniQuadWaveExtract
		ccccglobalmodes
"
ActiveThorns	= "CoMCheck"
comcheck::use_slab = "yes"
CoMCheck::check_every = 10240

#ActiveThorns = "Perturb"
#
#Perturb::pert_vel_m1 = "yes"
#Perturb::pert_vel_random = "no"
#Perturb::vel_pert_amp = 0.1
#Perturb::cyl_r_min= 0.0
#Perturb::cyl_r_max = 20.0
#Perturb::z_min = 20.0
#Perturb::z_max = 100.0
#Perturb::k_z = 33.7
#Perturb::m1_same_handedness = "yes"

# Spacetime evolution (comment out for cowling)
ActiveThorns = "	ML_BSSN
			ML_BSSN_Helper
			ML_ADMConstraints
			GenericFD
			NewRad
		"

#ActiveThorns = "CTGBase CTGEvolution CTGGauge CTGMatter
#                CTGConstraints CTGRadiativeBC SummationByParts GlobalDerivative"

LoopControl::settle_after_iteration = 0

############################# Flesh Data ##################################
Cactus::cctk_run_title = "symtest_nopert"
Cactus::cctk_full_warnings         = yes
Cactus::highlight_warning_messages = no
#Cactus::cctk_timer_output          = "full"

carpet::verbose = no
carpet::veryverbose = no #yes
carpet::barriers = no #"yes"

############################# Output Related ##################################
IO::out_dir              = $parfile
IO::checkpoint_dir       = "$parfile/cp"
IO::recover_dir       	 = "$parfile/cp"
#IO::recover_dir       	 = "/scratch/sciteam/pmoesta/simulations/e25b13-new-symm4cntm5/output-0000/$parfile/cp"
#IO::recover_dir       	 = "/scratch/sciteam/pmoesta/simulations/e25b13-new-symm4c/output-0000/$parfile/cp"
iohdf5::out_dir          = $parfile
IO::out_single_precision = yes

#--- checkpoint recovery
io::recover 					=	autoprobe	
iohdf5::checkpoint 				= 	yes
iohdf5::open_one_input_file_at_a_time 		= 	yes
io::checkpoint_every 				= 	40960
io::checkpoint_keep 				= 	1	
carpetiohdf5::use_reflevels_from_checkpoint 	= 	"yes"
iohdf5::skip_recover_variables	= 	"
carpetevolutionmask::evolution_mask
ZELMANITRACERS::tmass
ZELMANITRACERS::tracerevol
ZELMANITRACERS::tracervel
ZELMANITRACERS::tracervel_old
ZELMANITRACERS::first_step
"

IOBasic::outInfo_vars			=	"Carpet::physical_time_per_hour hydrobase::rho admbase::alp hydrobase::bvec GRHydro::divB hydrobase::temperature hydrobase::eps hydrobase::entropy"

carpetioscalar::outScalar_vars		=	"
		 	ML_ADMConstraints::ML_Ham{reductions='norm2 norm1 norm_inf'}
		 	ML_ADMConstraints::ML_Mom{reductions='norm2 norm1 norm_inf'}
			hydrobase::rho{reductions='minimum maximum'}
                        hydrobase::press{reductions='minimum maximum'}
                        hydrobase::eps{reductions='minimum maximum'}
                        hydrobase::vel{reductions='minimum maximum'}
			admbase::lapse{reductions='minimum maximum'}
			hydrobase::temperature{reductions='minimum maximum'}
			hydrobase::entropy{reductions='minimum maximum'}
			hydrobase::y_e{reductions='minimum maximum'}
			grhydro::tau{reductions='minimum maximum'}
			zelmanileak::zelmani_leak_account_local{reductions='sum'}
			"


carpetioascii::out1D_vars               =       "
		 	ML_ADMConstraints::ML_Ham
		 	ML_ADMConstraints::ML_Mom
			hydrobase::rho
                        hydrobase::press
                        hydrobase::eps
                        hydrobase::vel
                        hydrobase::y_e
                        hydrobase::temperature
			hydrobase::entropy
                        GRHydro::scon
                        GRHydro::dens
                        GRHydro::tau
			admbase::lapse
			admbase::metric
			admbase::shift
			admbase::curv 
			hydrobase::bvec
			GRHydro::bcom_sq
			GRHydro::divB
			"

carpetioascii::out0D_vars               =       "
			Carpet::physical_time_per_hour
			Carpet::timing
                        ccccglobalmodes::total_mass
                        ccccglobalmodes::center_of_mass
			hydrobase::temperature
			hydrobase::entropy
			hydrobase::y_e
			zelmanishocktracker2::shockmin
                        zelmanishocktracker2::shockav
                        zelmanishocktracker2::shockmax
			"

carpetioascii::out2D_vars               =       "
			hydrobase::temperature
			hydrobase::y_e
			grhydro::dens
			hydrobase::press
			hydrobase::eps
			hydrobase::entropy
			"

carpetiohdf5::out3D_vars               =       "
			hydrobase::temperature
			hydrobase::y_e
                        hydrobase::vel
                        hydrobase::rho
                        hydrobase::press
			hydrobase::eps
			hydrobase::entropy
			hydrobase::bvec
			GRHydro::bcom_sq
			zelmanileak::zelmani_leak_account_local
			admbase::lapse
			admbase::metric
			admbase::shift
			"

#--- output frequency

iohdf5::out_criterion 				= 	"divisor"
carpetioascii::out0D_criterion 			= 	"divisor"
carpetioascii::out1D_criterion 			=	"divisor"
carpetioascii::out2D_criterion 			= 	"divisor"
carpetioascii::out3D_criterion 			= 	"divisor"
carpetiohdf5::out3D_criterion 			= 	"divisor"
carpetioscalar::outScalar_criterion		=	"divisor"
carpetioscalar::outScalar_every			=	1024
ioascii::out0D_every				=	4096
IOASCII::out1D_every	     			=       20480
IOASCII::out2D_every				= 	-1	
iohdf5::out_every				=  	-1	
iohdf5::out3D_every				= 	512		

carpetiobasic::outInfo_every   			=	1
carpetiobasic::real_min      			= 	1.0e-2

iohdf5::one_file_per_group     = yes
IOASCII::one_file_per_group     = yes
IOASCII::output_symmetry_points = no
IOASCII::out1D_d                = no

############################# Driver ##################################

Carpet::domain_from_coordbase = yes
Carpet::max_refinement_levels = 9 

driver::ghost_size       = 3
Carpet::use_buffer_zones = yes
Carpet::use_overlap_zones = "yes"

Carpet::prolongation_order_space = 5
Carpet::prolongation_order_time  = 2

#Carpet::time_refinement_factors         = "[1,2,4,8,8,16,32,64,128,256,512,1024]"

Carpet::init_fill_timelevels = yes
InitBase::initial_data_setup_method = init_some_levels

Carpet::output_timers_every      = 20480
CarpetLib::print_timestats_every = 20480
CarpetLib::print_memstats_every  = 20480

CarpetInterp::check_tree_search         = no
CarpetInterp::tree_search               = yes

Carpet::enable_all_storage       = "no"

CarpetLib::interleave_communications = "yes"
CarpetLib::combine_sends             = "yes"

CarpetRegrid2::freeze_unaligned_parent_levels = "yes"
CarpetRegrid2::freeze_unaligned_levels = "yes"
CarpetRegrid2::snap_to_coarse          = "yes"

############################# Grid ##################################

CoordBase::domainsize = "minmax"
CoordBase::xmin       = -3840
CoordBase::ymin       = -3840
CoordBase::zmin       = -3840
CoordBase::xmax       = 3840
CoordBase::ymax       = 3840
CoordBase::zmax       = 3840

CoordBase::spacing  = "gridspacing"
CoordBase::dx = 64.0
CoordBase::dy = 64.0
CoordBase::dz = 64.0

CartGrid3D::type           = "coordbase"
CartGrid3D::domain         = "full"
CartGrid3D::avoid_originx  = no
CartGrid3D::avoid_originy  = no
CartGrid3D::avoid_originz  = no

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_z_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_upper     = 3

CarpetRegrid2::min_distance          =  0
CarpetRegrid2::boundary_shiftout     = 0
CarpetRegrid2::ensure_proper_nesting = yes
CarpetRegrid2::symmetry_rotating90 = no 

CarpetRegrid2::num_centres  = 1
CarpetRegrid2::num_levels_1 = 4
CarpetRegrid2::position_x_1 = 0
CarpetRegrid2::position_y_1 = 0
CarpetRegrid2::position_z_1 = 0
CarpetRegrid2::radius_1[1]   = 2073.6   # 
CarpetRegrid2::radius_1[2]   = 1651.2   # 
CarpetRegrid2::radius_1[3]   = 1056.0   # 
CarpetRegrid2::radius_1[4]   = 192.0    # 
CarpetRegrid2::radius_1[5]   = 144.0    # 
CarpetRegrid2::radius_1[6]   = 98.4     # 
CarpetRegrid2::radius_1[7]   = 40.0     # 
CarpetRegrid2::radius_1[8]   = 12.0     # 
CarpetRegrid2::radius_1[9]  =  6.0      # 
CarpetRegrid2::radius_1[10]  =  3.0     # 
CarpetRegrid2::radius_1[11]  =  2.0     # 

corecollapsecontrol::rho_max_list[0] = 8.0e10
corecollapsecontrol::rho_max_list[1] = 3.2e11
corecollapsecontrol::rho_max_list[2] = 1.28e12
corecollapsecontrol::rho_max_list[3] = 5.12e12
corecollapsecontrol::rho_max_list[4] = 2.048e13
corecollapsecontrol::rho_max_list[5] = 3.0e15
corecollapsecontrol::rho_max_list[6] = 5.0e15
corecollapsecontrol::rho_max_list[7] = 9.0e88
corecollapsecontrol::check_every = 128

corecollapsecontrol::rho_max_every = 128
CoreCollapseControl::handle_PMR = yes
CoreCollapseControl::output_control = yes
CoreCollapseControl::prebounce_rho = 1.0e13
CoreCollapsecontrol::bounce_rho = 2.0e14
corecollapsecontrol::bounce_criterion = "entropy"
corecollapsecontrol::bounce_entropy = 3.0e0
CoreCollapseControl::preb_out2D_every = -1 
CoreCollapseControl::preb_out1D_every = 4096 
CoreCollapseControl::preb_out0D_every = 64
CoreCollapseControl::preb_out3Dhdf5_every = 512 
CoreCollapseControl::preb_checkpoint_every = 4096
CoreCollapseControl::preb_outscalar_every = 64
CoreCollapseControl::preb_waves_every = 32

CoreCollapseControl::preBH_alpA = 0.4e0
CoreCollapseControl::preBH_alpB = 0.4e0
CoreCollapseControl::preBH_AH_every = 32
CoreCollapseControl::preBH_out3Dhdf5_every = -1
CoreCollapseControl::preBH_force_cooling_off = yes

CarpetRegrid2::regrid_every = 256 
CarpetRegrid2::movement_threshold_1 = 1.0e0
CarpetRegrid2::radius_rel_change_threshold_1 = 1.0e0

############################# Time Integration ##################################

MoL::ODE_Method             = "RK4"
MoL::MoL_Intermediate_Steps = 4
MoL::MoL_Num_Scratch_Levels = 1

Time::dtfac                 = 0.4

############################# Curvature Evolution ##################################

ADMBase::evolution_method               = "ML_BSSN"
ADMBase::lapse_evolution_method = "ML_BSSN"
ADMBase::shift_evolution_method = "ML_BSSN"
ADMBase::dtlapse_evolution_method = "ML_BSSN"
ADMBase::dtshift_evolution_method = "ML_BSSN"

ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"

ML_BSSN::timelevels = 3
ML_BSSN::my_initial_data = "ADMBase"
#GenericFD::FDscheme = "4th order centered macro"

ML_BSSN::my_initial_boundary_condition = "extrapolate-gammas"
ML_BSSN::my_boundary_condition = "none"
ML_BSSN::my_rhs_boundary_condition = "NewRad"
Boundary::radpower                 = 2               

ML_BSSN::harmonicN           = 1      # 1+log
ML_BSSN::harmonicF           = 2.0    # 1+log
ML_BSSN::ShiftGammaCoeff     = 0.75
ML_BSSN::AlphaDriver         = 0.0
ML_BSSN::BetaDriver          = 0.5
ML_BSSN::LapseAdvectionCoeff = 1.0
ML_BSSN::ShiftAdvectionCoeff = 1.0
ML_BSSN::LapseACoeff         = 0.0
ML_BSSN::ShiftBCoeff         = 1.0

ML_BSSN::MinimumLapse = 1.0e-8
ML_BSSN::ML_log_confac_bound = "none"
ML_BSSN::ML_metric_bound     = "none"
ML_BSSN::ML_Gamma_bound      = "none"
ML_BSSN::ML_trace_curv_bound = "none"
ML_BSSN::ML_curv_bound       = "none"
ML_BSSN::ML_lapse_bound      = "none"
ML_BSSN::ML_dtlapse_bound    = "none"
ML_BSSN::ML_shift_bound      = "none"
ML_BSSN::ML_dtshift_bound    = "none"

ML_BSSN::UseSpatialBetaDriver = "yes"
ML_BSSN::SpatialBetaDriverRadius = 50.0e0

Dissipation::epsdis = 0.1
Dissipation::epsdis2 = 0.0
Dissipation::epsdis_for_level2[6] = 0.1
Dissipation::epsdis_for_level2[5] = 0.01
#Dissipation::epsdis_for_level2[4] = 0.01
#Dissipation::epsdis_for_level2[3] = 0.01
#Dissipation::epsdis_for_level2[2] = 0.01
#Dissipation::epsdis_for_level2[1] = 0.01
#Dissipation::epsdis_for_level2[0] = 0.01
Dissipation::order = 5
Dissipation::vars                       = "
	ML_BSSN::ML_log_confac
        ML_BSSN::ML_metric
        ML_BSSN::ML_trace_curv
        ML_BSSN::ML_curv
        ML_BSSN::ML_Gamma
        ML_BSSN::ML_lapse
        ML_BSSN::ML_shift
        ML_BSSN::ML_dtlapse
        ML_BSSN::ML_dtshift
"
Dissipation::vars2                       = "
	GRHydro::Bcons
"

TmunuBase::stress_energy_storage = yes
TmunuBase::stress_energy_at_RHS  = yes
TmunuBase::prolongation_type = "none"
TmunuBase::support_old_CalcTmunu_mechanism = no

CartesianCoordinates::store_inverse_jacobian = yes
CartesianCoordinates::store_jacobian = yes
CartesianCoordinates::store_volume_form = "yes"

############################# Magneto-hydrodynamics ##################################

ADMMacros::spatial_order = 4

hydrobase::timelevels				=	3
hydrobase::evolution_method			=	"GRHydro"
HydroBase::Bvec_evolution_method 		= 	"GRHydro"
hydrobase::prolongation_type 			= 	"ENO"
HydroBase::initial_hydro			=	"starmapper"

### 	hot stuff

HydroBase::initial_temperature                  =       "zero"
HydroBase::initial_entropy                      =       "zero"
HydroBase::initial_y_e                          =       "one"
hydrobase::Y_e_evolution_method                 =       "GRHydro"
hydrobase::temperature_evolution_method         =       "GRHydro"

grhydro::grhydro_eos_type                       =       "General"
grhydro::grhydro_eos_table                      =       "nuc_eos"

GRHydro::reconstruct_temper            		=       "yes"
GRHydro::GRHydro_rho_central			=	6.4746716e-07	
GRHydro::riemann_solver				=	"HLLE"
#GRHydro::GRHydro_eos_type			=	"General"
#GRHydro::GRHydro_eos_table			=	"Hybrid"
GRHydro::recon_method	            		=       "weno"
#GRHydro::tvd_limiter	            		=       "vanLeerMC2"
GRHydro::use_enhanced_ppm                       =       "no"
GRHydro::ppm_detect				=	"yes"
GRHydro::GRHydro_stencil          		=       3
GRHydro::ppm_flatten                            =       stencil_3
#GRHydro::ppm_epsilon                            =       0.1 
GRHydro::bound		                	=       "flat"

grhydro::grhydro_y_e_min                        =       0.05 
grhydro::grhydro_y_e_max                        =       0.55 

GRHydro::use_cxx_code				= "yes"

GRHydro::clean_divergence          = "no"
GRHydro::transport_constraints     = "yes"
GRHydro::kap_dc                    = 0.0
GRHydro::track_divB                = "yes"
GRHydro::calculate_bcom            = "yes"

grhydro::evolve_tracer 				= 	"no"
grhydro::EoS_Change				=	"no"

GRHydro::Grhydro_MaxNumConstrainedVars 		= 33 
GRHydro::GRHydro_MaxNumEvolvedVars	     	= 10
grhydro::grhydro_maxnumsandrvars                = 16

grhydro::grhydro_c2p_warnlevel 			= 0
grhydro::grhydro_c2p_warn_from_reflevel		= 4
grhydro::con2prim_oct_hack			= no 

grhydro::grhydro_perc_ptol                      =       1.0e-10
grhydro::grhydro_eos_rf_prec                    =       1.0e-10
grhydro::grhydro_eos_hot_eps_fix		=	yes
grhydro::GRHydro_c2p_reset_eps_tau_hot_eos	=	yes

#------ Atmosphere treatment

SpaceMask::use_mask 				= 	yes
GRHydro::rho_rel_min				=	1e-8
grhydro::rho_abs_min                            =       1.61930347e-14
GRHydro::GRHydro_atmo_tolerance			=	0.001
#GRHydro::GRHydro_ppm_atmo_tolerance             = 0 #1e5 #1e3

#------ Atmosphere only for
#GRHydro::initial_atmosphere_factor		=	1.0
#GRHydro::initial_rho_abs_min			=	1e-16


##### Iniitial data

admbase::initial_data				=	"starmapper"
admbase::initial_lapse				=	"starmapper"
admbase::initial_shift				=	"starmapper"

zelmanistarmapper::Num_Radial                   = 	511	
zelmanistarmapper::Num_Spacetime                =       20000
zelmanistarmapper::use_full_profile_for_pot     =       yes
zelmanistarmapper::initial_vel                  =       yes

zelmanistarmapper::Profile_File                 =       "/u/sciteam/pmoesta/E25.txt.short"
#zelmanistarmapper::Profile_File                 =       "/panfs/ds06/sxs/pmoesta/simulations/E25.txt.short"
#zelmanistarmapper::Profile_File                 =       "/lustre/scratch/moesta/simulations/E25.txt.short"
zelmanistarmapper::Profile_Type                 =       "HegerShort"

zelmanistarmapper::Starmapper_EoS               =       "Omni"
zelmanistarmapper::Starmapper_eoskey            =       4
zelmanistarmapper::Hybrid_Gamma                 =       1.333333333333333e0
zelmanistarmapper::Hybrid_K                     =       4.93483302030614e14

zelmanistarmapper::Rotation_Type                =       "rotlaw"
zelmanistarmapper::RotLaw_Type                  =       "MM_D"
zelmanistarmapper::starmapper_rho_min           =       0.0e0
#zelmanistarmapper::RotLaw_Omega0                =       0.0e0
#zelmanistarmapper::RotLaw_A                     =       677.141e0

##### for rot0:

zelmanistarmapper::RotLaw_Omega0                =       1.379310344827586e-05
#zelmanistarmapper::RotLaw_A                     =       338.61573885e0
zelmanistarmapper::RotLaw_X                     =       338.61573885e0
#zelmanistarmapper::RotLaw_Z                     =       1354.462955438e0 
zelmanistarmapper::RotLaw_Z                     =       677.231477719e0 
zelmanistarmapper::RotLaw_GRB_rtrans            =       1.95e8
zelmanistarmapper::RotLaw_GRB_dropfac           =       3.0e0
zelmanistarmapper::RotLaw_GRB_drmatch           =       1.0e7

HydroBase::initial_Bvec          = "Takiwaki"
GRHydro_InitData::poloidal_A_b      = 1.21816074646183519299e-7
GRHydro_InitData::takiwaki_r0      = 1354.462955 
#GRHydro_InitData::takiwaki_r0      = 337.8378


GRHydro_InitData::poloidal_n_p      = 1 
GRHydro_InitData::poloidal_P_cut    = 5.0e-17
GRHydro_InitData::poloidal_rho_max  = 1.61930347e-08

eos_omni::nuceos_read_table = yes
eos_omni::nuceos_table_name = "/u/sciteam/pmoesta/LS220_234r_136t_50y_analmu_20091212_SVNr26.h5"
#eos_omni::nuceos_table_name = "/panfs/ds06/sxs/pmoesta/simulations/LS220_234r_136t_50y_analmu_20091212_SVNr26.h5"
#eos_omni::nuceos_table_name = "/u/sciteam/srichers/eos_tables/LS220_234r_136t_50y_analmu_20091212_SVNr26.h5"
#eos_omni::nuceos_table_name = "/lustre/scratch/moesta/simulations/LS220_234r_136t_50y_analmu_20091212_SVNr26.h5"
eos_omni::do_energy_shift = yes

eos_omni::poly_gamma                            = 5.0
eos_omni::poly_k                                = 0.4640517
eos_omni::gl_gamma                              = 5.0
eos_omni::gl_k                                  = 0.4640517
eos_omni::hybrid_gamma1                         = 5.0
eos_omni::hybrid_gamma2                         = 2.4
eos_omni::hybrid_gamma_th                       = 1.333333333333333333
eos_omni::hybrid_k1                             = 0.4640517
eos_omni::hybrid_rho_nuc                        = 3.238607e-4

ZelmaniLeak::interpolator = "uniform cartesian from center" # from LocalInterp, don't use AEILocalInterp's interpolator
ZelmaniLeak::interpolator_options = "order=1"

ZelmaniLeak::update_tau_every = 64
ZelmaniLeak::do_tau = yes
ZelmaniLeak::do_heat = yes
ZelmaniLeak::ntheta = 40
ZelmaniLeak::nphi = 20
ZelmaniLeak::nrad = 600
ZelmaniLeak::rad_max = 200.0
ZelmaniLeak::nrad_outer = 200
ZelmaniLeak::rad_max_outer = 2000.0
ZelmaniLeak::symm = "full"
ZelmaniLeak::leak_in_prebounce = no
ZelmaniLeak::start_with_clean_tau =no 

ZelmaniLeak::do_ye_of_rho = yes
ZelmaniLeak::do_pnu = yes
ZelmaniLeak::pnu_rho_start = 2.0e12
ZelmaniLeak::pnu_in_tmunu = yes
ZelmaniLeak::min_temp = 0.1

# ye of rho parameters
ZelmaniLeak::yeofrho_rho1 = 1.0e7
ZelmaniLeak::yeofrho_rho2 = 1.0e13
ZelmaniLeak::yeofrho_ye1 = 0.5e0
ZelmaniLeak::yeofrho_ye2 = 0.29e0
ZelmaniLeak::yeofrho_yec = 0.035e0
ZelmaniLeak::yeofrho_do_high_correction = yes
ZelmaniLeak::yeofrho_high_correction_ye = 0.2717e0
ZelmaniLeak::yeofrho_high_correction_rho = 2.55e14

ZelmaniLeak::do_ye_of_rho_from_profile = no
ZelmaniLeak::Profile_File = "/home/cott/Cactus/s20rhoyeprof_garching.dat"
ZelmaniLeak::Profile_Zones = 366
ZelmaniLeak::Zones = 1000

########################## Horizons #####################################

SphericalSurface::nsurfaces = 3
SphericalSurface::maxntheta = 39
SphericalSurface::maxnphi   = 76

SphericalSurface::ntheta       [0] = 39
SphericalSurface::nphi         [0] = 76
SphericalSurface::nghoststheta [0] = 2
SphericalSurface::nghostsphi   [0] = 2
SphericalSurface::set_spherical[0] = yes
SphericalSurface::radius       [0] = 1500.

SphericalSurface::ntheta       [1] = 39
SphericalSurface::nphi         [1] = 76
SphericalSurface::nghoststheta [1] = 2
SphericalSurface::nghostsphi   [1] = 2
SphericalSurface::set_spherical[1] = yes
SphericalSurface::radius       [1] = 2000.

SphericalSurface::ntheta       [2] = 39
SphericalSurface::nphi         [2] = 76
SphericalSurface::nghoststheta [2] = 2
SphericalSurface::nghostsphi   [2] = 2
SphericalSurface::set_spherical[2] = yes
SphericalSurface::radius       [2] = 1000.

############################# Infrastructure Related Stuff ##################
# Timing parameters
#TimerReport::out_every    = 40960
#TimerReport::out_filename = "TimerReport"

#--- nanchecker

nanchecker::check_every 			= 	64
nanchecker::check_vars 				= 	"admbase::alp admbase::gxx admbase::kxx GRHydro::dens hydrobase::bvec"
nanchecker::action_if_found 			= 	"abort"
nanchecker::verbose = "all"

Cactus::terminate                               =       "never"
Cactus::cctk_itlast				=       40960
io::checkpoint_on_terminate                     =       "yes"

TriggerTerminationManual::on_remaining_walltime =       15
TriggerTerminationManual::max_walltime = 12.0
TriggerTerminationManual::termination_file      =       "terminate"
TriggerTerminationManual::check_file_every      =       64
TriggerTerminationManual::termination_from_file =       yes

ccccglobalmodes::do_CoM         = yes
ccccglobalmodes::cut_method     = radius
#ccccglobalmodes::radius_cut     = 500
ccccglobalmodes::CoM_radius     = 1500
CCCCGlobalModes::compute_every = 128

ZelmaniShockTracker2::do_regrid_z = "yes" 
ZelmaniShockTracker2::radius_xy = 120.0
ZelmaniShockTracker2::track_n_levels = 1
ZelmaniShockTracker2::track_levels[0] = 6
ZelmaniShockTracker2::track_level_type[0] = 0
ZelmaniShockTracker2::track_level_out_dist[0] = 13.0
ZelmaniShockTracker2::track_level_dist_fac = 1.2

ZelmaniShockTracker2::ntheta = 21
ZelmaniShockTracker2::nphi = 21
ZelmaniShockTracker2::nrad = 800
ZelmaniShockTracker2::rad_max = 300.0
ZelmaniShockTracker2::nrad_outer = 200
ZelmaniShockTracker2::rad_max_outer = 1000.0
ZelmaniShockTracker2::symm = "full"

ZelmaniShockTracker2::start_time_inner_rad = 800.0
ZelmaniShockTracker2::inner_min_radius = 40.0
ZelmaniShockTracker2::verbose = yes
ZelmaniShockTracker2::compute_every = 64
ZelmaniShockTracker2::check_tracking_every = 64
ZelmaniShockTracker2::zst_regrid_every = 64

ActiveThorns = "ZelmaniTracers"

zelmanitracers::reset_tracers_on_recover = "yes"
zelmanitracers::ntracers = 20000
zelmanitracers::tracers_with_leakage = "yes"
zelmanitracers::evolve_every = 1
zelmanitracers::interpolator = "Lagrange polynomial interpolation"
zelmanitracers::interpolator_options = "order=2"

zelmanitracers::tracerproblem = "StellarCore"

zelmanitracers::rad_max[0] = 500.0
zelmanitracers::rad_min[0] = 20.0

zelmanitracers::nradialshells = 50
zelmanitracers::ntheta = 20
zelmanitracers::nphi = 20
zelmanitracers::randomize = "yes"
zelmanitracers::output_extra_fields = "yes"



#ActiveThorns = "ZelmaniCoMShift driftcorrectcom"
#ActiveThorns = "ZelmaniCoMShift"
#ZelmaniCoMShift::use_CoM_radius = yes
#ZelmaniCoMShift::CoM_radius = 100.0
#ZelmaniCoMShift::verbose_level = 1
#ZelmaniCoMShift::do_every = 128 
#
#driftcorrectcom::verbose_level = 1
#driftcorrectcom::first_activation_on_recovery = no
#
#driftcorrectcom::do_position_correction = yes
#driftcorrectcom::position_timescale = 50.75
#driftcorrectcom::position_correction_falloff = 0.0
