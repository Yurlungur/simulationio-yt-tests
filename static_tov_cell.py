import yt
import os
import re
import pysimulationio
from pysimulationio import SimulationIO as SIO
from pysimulationio import RegionCalculus as RC
from pysimulationio import H5
import numpy as np

import warnings

# warnings.filterwarnings('error')

ghosts = True

#base_dir='/home/jmiller/programming/SimulationIO-yt-tests'
#source_dir=base_dir+'/'+'cactus'
source_dir = '/home/jmiller/programming/yt-data'
if ghosts:
    fname='static-tov-cell-centred'#fname='iof5-static-tov-cell-centred'
else:
    fname='iof5-static-tov-no-ghosts'

fname += '.s5'
fpath=source_dir+'/'+fname

iteration = 0
timelevel = 0

config_template=re.compile('(iteration.)(0*)({}-timelevel.)(0*)({}\Z)'.format(iteration,timelevel))
config_template.search('iteration.00-timelevel.00').group(0)
configurationname='iteration.0000000000-timelevel.0'
print fpath
print configurationname

ds = yt.load(fpath,configuration=configurationname)

prj = yt.ProjectionPlot(ds, 'z','metric_volume_form',center=[120,120,0],width=240)
prj.set_cmap(field="all", cmap="jet")
prj.set_log('metric_volume_form',False)
prj.annotate_grids()
prj.show()
prj.save('projectionplot.png')
