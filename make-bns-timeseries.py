import time
import os
import H5
import SimulationIO as SIO
import RegionCalculus as RC
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import yt
yt.enable_parallelism()

num_files=16
path_base='/scratch/sciteam/miller1/davids_simulations/LS220_M135135_LK'
file_template='bns-{0:>4}.s5'
path_template = path_base + '/' + file_template
projection_template = 'bns-it-{}-projection.png'
slice_template = 'bns-it-{}-slice.png'

def get_configurations(fpath):
    f = H5.H5File(fpath,H5.H5F_ACC_RDONLY)
    p = SIO.readProject(f)
    cnames = filter(lambda x: x != 'global',
                    list(p.configurations.iterkeys()))
    f.close()
    iterations = map(lambda x: int(x.split('.')[1].split('-')[0]),
                     cnames)
    return iterations,cnames


