#!/usr/local/anaconda/bin/python2

# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2016-05-03 21:07:24 (jmiller)>

# Tests how quickly we can explore the dataset.
# purely benchmarks file IO.

# This benchmark is run with a "hot" cache.

# path
import sys
sys.path.append('/nfs/galaxy/unixhome/jmiller/SimulationIO')

# disable openmp
import os
os.environ['OMP_NUM_THREADS']=str(1)

# remaining imports
import yt
import H5
import re
import SimulationIO as SIO
import RegionCalculus as RC
import numpy as np
import time

out_file_name = "yt-benchmark-io.dat"
num_times = 3

num_cells_list = [64,128,256,512]
amplitude = 0.05
iteration = 0
path_template=("random-hashed-nc{}-hs0-amp{:>0.4}-i0000"
               +"/output-0000/random-hashed/"
               +"radhydro2-ustate.s5")
configuration_template="iteration.{:0>10}-timelevel.{}"

# assume 2 ghost zones
# assume dx = 1
num_ghosts = 2
domain_left_edge = [-2,-2,-2]
domain_dds = [1,1,1]

# field to read/plot:
field = 'RADHYDRO2::ustate[0]'

def get_path_configuration(num_cells):
    path_name=path_template.format(num_cells, amplitude)
    configuration_name = configuration_template.format(iteration, 0)
    print "path name = ",path_name
    print "configuration name = ",configuration_name
    return path_name,configuration_name

def explore_data(num_cells):
    a = None
    path_name,configuration_name = get_path_configuration(num_cells)
    ds = yt.load(path_name, ghost_zones = num_ghosts,
                 domain_left_edge = domain_left_edge,
                 domain_dds = domain_dds,
                 periodicity=(True,True,True),
                 configuration=configuration_name)
    
    for g in ds.index.grids:
        a = g[field]
        a = 2*a + a**2
        print "max = ",np.max(np.abs(a))
        g.clear_data()
    return

def print_time(num_cells,dt):
    with open(out_file_name,'a') as f:
        f.write("{} {}\n".format(num_cells,dt))

def create_file():
    with open(out_file_name,'w') as f:
        f.write("# num_cells time (seconds)\n")

def benchmark_ds(num_cells):
    explore_data(num_cells)
    start = time.clock() # in seconds
    for i in range(num_times):
        explore_data(num_cells)
    end = time.clock() # in seconds
    dt = (end - start) / float(num_times)
    print_time(num_cells,dt)

if __name__ == "__main__":
    create_file()
    for nc in num_cells_list:
        benchmark_ds(nc)
