#!/bin/bash
#PBS -l nodes=4:ppn=32:xe
#PBS -l walltime=12:00:00
#PBS -N yt batch
#PBS -e $PBS_JOBID.err
#PBS -o $PBS_JOBID.out
#PBS -m bea
#PBS -M jmiller@perimeterinstitute.ca

. /opt/modules/default/init/bash

module unload PrgEnv-cray
module load PrgEnv-gnu
module load cray-hdf5/1.8.16

# anaconda
export PATH=/u/sciteam/miller1/anaconda2/bin:${PATH}
export MY_LIBRARY_PATH=/u/sciteam/miller1/anaconda2/lib:/u/sciteam/miller1/anaconda2/lib/python2.7:${LD_LIBRARY_PATH}
export PYTHONPATH=/u/sciteam/miller1/SimulationIO:${PYTHONPATH}

mkdir -p /scratch/sciteam/$USER/$PBS_JOBID
cd /scratch/sciteam/$USER/$PBS_JOBID

LD_LIBRARY_PATH=${MY_LIBRARY_PATH} aprun -n 16 -N 4 -d 2 python /u/sciteam/miller1/bns-make-plots-parallel.py
