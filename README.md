# Test Cases for SimulationIO and yt

### Authors

* Jonah Miller <jonah.maxwell.miller@gmail.com>
* Erik Schnetter <eschnetter@perimeterinstitute.ca>
* The yt community

### Details

* These are test cases for the simulationio reader of the yt project. Note that they're pretty rough around the edges.
* SimulationIO is a project by Erik Schnetter which can be found
  [here](https://github.com/eschnett/SimulationIO). The goal is to
  develop a general self-describing format for relativistic
  simulations. The metadata uses category theory to describe the
  topological and geometric structure of the grids, the coordinate
  system, the manifold, etc. We hope to use it as a target for Carpet
  data.
* The reader for yt can be found
  [here](https://bitbucket.org/Yurlungur/yt-simulationio).